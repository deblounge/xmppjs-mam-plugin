# xmppjs-mam-plugin


**Plugin for [xmpp.js](https://github.com/xmppjs/xmpp.js) to provide Message Archiving Management ([XEP-0313]( http://xmpp.org/extensions/xep-0313.html)).**


> XMPP is the open standard for messaging and presence, a set of open technologies for realtime messaging, presence, multi-party chat, voice and video calls, to name a few. 
> Read more on  [xmpp.org/about-xmpp/technology-overview/](https://xmpp.org/about/technology-overview.html)

> **[xmpp.js](https://github.com/xmppjs/xmpp.js)** is a JavaScript library for [XMPP](http://xmpp.org/).


## Install

npm install xmppjs-mam-plugin

## Motivation
The current implementation of [xmpp.js](https://github.com/xmppjs/xmpp.js) lacks [XEP-0313]( http://xmpp.org/extensions/xep-0313.html) support, and this is much desired by a lot of developers building projects depending on [xmpp.js](https://github.com/xmppjs/xmpp.js).
For instance, [Matrix-bifrost](https://github.com/matrix-org/matrix-bifrost) is a general purpose XMPP-Matrix bidirectional bridge commonly used to connect  matrix clients to XMPP servers and vice versa.
[Matrix-bifrost](https://github.com/matrix-org/matrix-bifrost) depends on [xmpp.js](https://github.com/xmppjs/xmpp.js)  and currently lacks Message Archive Management, users joining Matrix rooms from XMPP servers are not able to see offline messages.  This plugin seeks to be a reliable abstraction of [XEP-0313]( http://xmpp.org/extensions/xep-0313.html) for use with [xmpp.js](https://github.com/xmppjs/xmpp.js) both in the browser and in the server

## Contributing

## credits

## License
GNU LGPLv3

